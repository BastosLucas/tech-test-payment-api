using MediatR;
using tech_test_payment_api.Application.Dto;

namespace tech_test_payment_api.Application.Command.Vendas
{
    public class AddVendaCommand : IRequest<VendaDto>
    {
        public VendaRequest? Venda { get; set; }                
    }
}