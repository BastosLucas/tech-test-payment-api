using AutoMapper;
using MediatR;
using tech_test_payment_api.Application.Dto;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.ValueObjects;

namespace tech_test_payment_api.Application.Command.Vendas
{
    public class VendaHandler : IRequestHandler<AddVendaCommand, VendaDto>
    {
        public ILogger<VendaHandler> Logger { get; }
        public IMapper Mapper { get; }
        public VendasPersistence Vendas { get; set; }

        public VendaHandler(ILogger<VendaHandler> logger, IMapper mapper, VendasPersistence vendas)
        {
            Logger = logger;
            Mapper = mapper;
            Vendas = vendas;
        }

        public async Task<VendaDto> Handle(AddVendaCommand request, CancellationToken cancellationToken)
        {
            Logger.LogInformation("Adiciona venda ao mecanismo de persistencia");
            var vendaResult = Mapper.Map<Venda>(request.Venda);

            Vendas?.ListVenda?.Add(vendaResult);

            Logger.LogInformation($"request recebida: {vendaResult?.Vendedor?.Nome}", Vendas);

            return await Task.FromResult(new VendaDto 
            {
                Message = "Venda adicionada copm sucesso",
                VendaId = vendaResult.Id
            });           
        }
    }
}