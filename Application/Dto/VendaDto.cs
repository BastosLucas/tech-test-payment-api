namespace tech_test_payment_api.Application.Dto
{
    public class VendaDto
    {
        public string? Message { get; set; }
        public Guid VendaId { get; set; }
    }
}