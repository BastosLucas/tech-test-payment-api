using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Application.Dto
{
    public class VendaPorIdDto
    {
        public GetVendaDto Venda { get; set; }
    }
}