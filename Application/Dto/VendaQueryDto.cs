using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Application.Dto
{
    public class VendaQueryDto
    {
        public List<GetVendaDto>? Vendas { get; set; }
    }
}