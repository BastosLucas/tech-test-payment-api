using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Application.Dto
{
    public class VendaRequest
    {
        public Vendedor? Vendedor { get; set; }
        public List<Produto>? Produtos { get; set; }
    }
}