using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using tech_test_payment_api.Application.Dto;
using tech_test_payment_api.Domain.ValueObjects;

namespace tech_test_payment_api.Application.Query.GetVendaId
{
    public class GetVendaPorIdHandler : IRequestHandler<GetVendaPorIdQuery, VendaPorIdDto>
    {
        private ILogger<GetVendaPorIdHandler> Logger { get; }
        private VendasPersistence Vendas { get; }

        public GetVendaPorIdHandler(ILogger<GetVendaPorIdHandler> logger, VendasPersistence vendas)
        {
            Logger = logger;
            Vendas = vendas;
        }

        public async Task<VendaPorIdDto> Handle(GetVendaPorIdQuery request, CancellationToken cancellationToken)
        {
            var id = Guid.Parse(request.VendaId);
            var vendas = Vendas?.ListVenda?.FirstOrDefault(x => x.Id == id);
            var vendaResult = VendaIdAdapter.Adapt(vendas);
            return await Task.FromResult(vendaResult);
        }
    }
}