using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using tech_test_payment_api.Application.Dto;

namespace tech_test_payment_api.Application.Query.GetVendaId
{
    public class GetVendaPorIdQuery : IRequest<VendaPorIdDto>
    {
        public string VendaId { get; set; }
    }
}