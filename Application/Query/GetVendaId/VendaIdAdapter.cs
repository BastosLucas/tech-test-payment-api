using tech_test_payment_api.Application.Dto;
using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Application.Query.GetVendaId
{
    public static class VendaIdAdapter
    {
        public static VendaPorIdDto Adapt(Venda? venda)
        {
            return new VendaPorIdDto
            {
                Venda = new GetVendaDto
                {
                    Id = venda.Id,
                    Produtos = venda.Produtos,
                    Vendedor = venda.Vendedor,
                    Status = venda.Status.ToString()
                }
            };
        }
    }
}