using AutoMapper;
using MediatR;
using tech_test_payment_api.Application.Dto;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.ValueObjects;

namespace tech_test_payment_api.Application.Query.VendaQuery
{
    public class VendaQueryHandler : IRequestHandler<VendaQuery, VendaQueryDto>
    {
        public ILogger<VendaQueryHandler> Logger { get; }
        public IMapper Mapper { get; }
        public VendasPersistence Vendas { get; set; }

        public VendaQueryHandler(ILogger<VendaQueryHandler> logger, IMapper mapper, VendasPersistence vendas)
        {
            Logger = logger;
            Mapper = mapper;
            Vendas = vendas;
        }

        public Task<VendaQueryDto> Handle(VendaQuery request, CancellationToken cancellationToken)
        {
            Logger.LogInformation("Obtendo todas as vendas");
            var vendasResult = Mapper.Map<List<Venda>, List<GetVendaDto>>(Vendas.ListVenda);
            return Task.FromResult(new VendaQueryDto {
                Vendas = vendasResult
            });
        }
    }
}