using AutoMapper;
using tech_test_payment_api.Application.Dto;
using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Application.Mappings.Profiles
{
    public class GetVendasProfile : Profile
    {
        public GetVendasProfile()
        {
            CreateMap<Venda, GetVendaDto>()
            .ForPath(src => src.Status, dest => dest.MapFrom(d => d.Status.ToString()));
        }
    }
}