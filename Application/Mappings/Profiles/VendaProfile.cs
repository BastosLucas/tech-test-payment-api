using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.Application.Dto;
using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Application.Mappings.Profiles
{
    public class VendaProfile : Profile
    {
        public VendaProfile()
        {
            CreateMap<VendaRequest, Venda>()
            .ForMember(dest => dest.Id, opt => opt.Ignore())
            .ForMember(dest => dest.Status, opt => opt.Ignore());
        }        
    }
}