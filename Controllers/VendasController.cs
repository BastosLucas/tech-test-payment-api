using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Application.Command.Vendas;
using tech_test_payment_api.Application.Dto;
using tech_test_payment_api.Application.Query.GetVendaId;
using tech_test_payment_api.Application.Query.VendaQuery;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        public IMediator Mediator { get; }

        public VendasController(IMediator mediator)
        {
            Mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<VendaQueryDto>> GetAll(CancellationToken cancellationToken)
        {
            var result = await Mediator.Send(new VendaQuery(), cancellationToken);
            return result;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<VendaPorIdDto>> GetById(string id, CancellationToken cancellationToken)
        {
            var request = new GetVendaPorIdQuery()
            {
                VendaId = id
            };
            var result = await Mediator.Send(request, cancellationToken);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult<VendaDto>> AddVenda([FromBody] AddVendaCommand request, CancellationToken cancellationToken)
        {
            var result = await Mediator.Send(request, cancellationToken);

            return result;
        }
    }
}