using System.Reflection;
using MediatR;
using tech_test_payment_api.Domain.ValueObjects;

namespace tech_test_payment_api.Extensions
{
    public static class IoCExtension
    {
        public static void AddIoc(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSingleton<VendasPersistence>();
        }
    }
}