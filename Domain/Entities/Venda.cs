using tech_test_payment_api.Domain.ValueObjects;

namespace tech_test_payment_api.Domain.Entities
{
    public class Venda
    {
        public Venda()
        {
            Id = Guid.NewGuid();
            Status = StatusVenda.AguardandoPagaemnto;
        }

        public Guid Id { get; set; }
        public Vendedor? Vendedor { get; set; }
        public List<Produto>? Produtos { get; set; }
        public StatusVenda Status { get; set; }     
    }
}