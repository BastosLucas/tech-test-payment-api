namespace tech_test_payment_api.Domain.ValueObjects
{
    public enum StatusVenda
    {
        AguardandoPagaemnto,
        PagamentoAprovado,
        EnviadoTransportadora,
        Entregue,
        Cancelado
    }
}