using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Domain.ValueObjects
{
    public class VendasPersistence
    {
        public List<Venda>? ListVenda { get; set; }

        public VendasPersistence()
        {
            ListVenda = new List<Venda>();
        }        
    }
}